package org.xe.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyFileUtil {

	// Environment value fetched from run config and 'qa' being the default value
	private static final String ENV = System.getProperty("env", "qa");
	
	public static Properties loadProperties() {
	    InputStream inputStream = PropertyFileUtil.class.getClassLoader().getResourceAsStream("config/"+ENV +".properties");
		
		Properties appProps = new Properties();
		try {
			appProps.load(inputStream);
		} catch (IOException e) {
			// TODO
			e.printStackTrace();
		}
		
		return appProps;
	}

}

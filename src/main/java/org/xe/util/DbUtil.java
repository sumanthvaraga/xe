package org.xe.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DbUtil {

	private static final Logger log = LogManager.getLogger(DbUtil.class);
	private Connection connection;
	
	public void getJdbcConn() {
		String databaseURL = System.getProperty("dburl");
		String user = System.getProperty("db.username");
		String password = System.getProperty("db.password");

		connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("Connecting to Database...");
			connection = DriverManager.getConnection(databaseURL, user, password);
			if (connection != null) {
				log.info("Connected to the Database...");
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}
	
	public void getMongoDbConn() {
		// Implementaion
	}
	
	public void cleanup(Connection con, Statement st, ResultSet rs) {
		  try {
		    // Release the Resources 
		    if (rs != null)
		        rs.close();
		    if (st != null)
		        st.close(); 
		    if (con != null)
		        con.close();
		   } catch (Exception e) { 
		        e.printStackTrace();
		   }
		 }
}

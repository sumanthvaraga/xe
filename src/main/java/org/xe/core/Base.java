package org.xe.core;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.xe.util.PropertyFileUtil;

public class Base {

	private Properties envConfig;
	private Driver driverObj;

	public Base() {
		init();
	}

	private void init() {

		envConfig = PropertyFileUtil.loadProperties();
		String browser = (String) envConfig.get("browser");

		switch (Browser.valueOf(browser.toUpperCase())) {

		case EDGE:

			driverObj = new IEdgeDriver();
			driverObj.createDriver();
			break;
			
		case FIREFOX:

			// <TODO>
			break;
			
		case CHROME:
		default:

			driverObj = new IChromeDriver();
			driverObj.createDriver();
			break;

		}
	}

	public Properties getProps() {
		return envConfig;
	}

	public WebDriver getDriver() {
		return driverObj.getDriver();
	}

}

package org.xe.core;

public enum Browser {
	
	CHROME("chrome"), FIREFOX("firefox"), EDGE("edge");
	
	private String value;

	Browser(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}

}

package org.xe.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

abstract class Driver {
	
	protected WebDriver driver;
	
	public DesiredCapabilities buildCapabilites() {
		//<TODO>
		return null;
	}
	
	public WebDriver getDriver() {
		return this.driver;
	}

	protected abstract void createDriver();

}

package org.xe.base;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.xe.core.Base;
import org.xe.test.ConversionTest;
import org.xe.util.ExtentManager;

/**
 * The TestBase class is the base class to fetch environment specific
 * configuration parameters from Jenkins/Maven. Based on the parameters, it
 * performs the browser setup and tear-down functions.
 * 
 * @author
 */

public class TestBase {

	protected static WebDriver driver;
	public static Properties envConfig;
	private static final Logger log = LogManager.getLogger(ConversionTest.class);

	@BeforeMethod()
	public void loadBaseUrl(Method method) {
		Base base = new Base();
		driver = base.getDriver();

		// Setting implicit wait
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		envConfig = base.getProps();
		driver.get(envConfig.getProperty("baseUrl"));
	}

	// Incase of failure a screenshot is taken
	@AfterMethod(alwaysRun = true)
	public void screenshotAndDeleteCookies(ITestResult testResult) throws IOException {
		// Taking screenshot in case of failure
		if (testResult.getStatus() == ITestResult.FAILURE) {
			log.error("Test has failed. Saving Screenshot");
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File("errorScreenshots\\" + testResult.getName() + "-"
					+ Arrays.toString(testResult.getParameters()) + ".jpg"));
		}

		// Deleting cookies
		driver.manage().deleteAllCookies();
		driver.quit();
		ExtentManager.extentReports.flush();
	}

//
//	@BeforeSuite
//	public void suiteSetup() throws Exception {
//	}
// 
//    @AfterSuite (alwaysRun = true)
//    public void suiteTearDown() {
//    }

}

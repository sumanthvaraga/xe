package org.xe.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.xe.base.TestBase;
import org.xe.pages.ConvertPage;
import org.xe.pages.CurrencyConverterPage;
import org.xe.util.ExcelUtil;
import org.xe.util.ExtentTestManager;

/**
 * This test is an example of test data being retrieved from an external file like Excel sheet
 * @author sumanth
 *
 */

public class ConversionTest2 extends TestBase{	
	
	@DataProvider(name = "tempTestData")
	public Object[][] tempTestData() throws Exception {

		String[][] testData = ExcelUtil.getExcelDataIn2DArray("./CCTestData.xlsx", "loginSheet");
		return testData;
	}
	
	//Test to demo data provider functionality
	@Test(dataProvider = "tempTestData")
	public void CurrencyConversionTest(String amount, String fromCurrency, String toCurrency) throws InterruptedException {
		ExtentTestManager.startTest("CurrenyConverterTest with DataProvider", "Converting the currencies and validating");
		
		CurrencyConverterPage ccPage = new CurrencyConverterPage(driver);
		ConvertPage convertPage = new ConvertPage(driver);
		
		ccPage.acceptCookie();
		ccPage.setAmount(amount);
		ccPage.setFromCurrency(fromCurrency);
		ccPage.setToCurrency(toCurrency);
		ccPage.clickConvert();
		
		convertPage.validateConversion();
	}

}

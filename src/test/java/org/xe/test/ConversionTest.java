package org.xe.test;

import org.apache.logging.log4j.LogManager;

import org.apache.logging.log4j.Logger;
import org.testng.annotations.Test;
import org.xe.base.TestBase;
import org.xe.pages.ConvertPage;
import org.xe.pages.CurrencyConverterPage;
import org.xe.util.ExtentTestManager;

/**
 * This test is an example of test data being hard coded directly in the test case.
 * @author sumanth
 *
 */

public class ConversionTest extends TestBase{

	private static final Logger log = LogManager.getLogger(ConversionTest.class);
	
	@Test
	public void CurrencyconverterTest() throws InterruptedException{
		ExtentTestManager.startTest("CurrencyconverterTest", "Converting the currencies and validating");
		
		log.info("Starting the Test");
		
		CurrencyConverterPage ccPage = new CurrencyConverterPage(driver);
		ConvertPage convertPage = new ConvertPage(driver);
		
		ccPage.acceptCookie();
		ccPage.setAmount("5.00");
		ccPage.setFromCurrency("AUD");
		ccPage.setToCurrency("EUR");
		ccPage.clickConvert();
		
		convertPage.validateConversion();
		
	}

}

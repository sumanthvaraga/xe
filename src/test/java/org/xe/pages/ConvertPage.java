package org.xe.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class ConvertPage {
	
	public ConvertPage(WebDriver driver) {
		this.driver = driver;
        PageFactory.initElements(driver, this);
	}

	private WebDriver driver;
	
	private static final Logger log = LogManager.getLogger(ConvertPage.class);
	
	@FindBy(xpath="//div[contains(@class,'unit-rates___Styled')]/p")
	private WebElement convertedCurrency;
	
	@FindBy(xpath="//section/div/h1[contains(@class,'heading__Heading')]")
	private WebElement currencyHeader;
	
	public ConvertPage validateConversion() {
		log.info("Validating the conversion");
		
		Assert.assertEquals("Expected Result", "Actual Result");
		//<TODO>
		return new ConvertPage(driver);
	}
	
	
}

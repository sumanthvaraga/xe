package org.xe.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CurrencyConverterPage {
	
	public CurrencyConverterPage(WebDriver driver) {
		this.driver = driver;
        PageFactory.initElements(driver, this);
	}

	WebDriver driver;
	
	private static final Logger log = LogManager.getLogger(CurrencyConverterPage.class);
	
	@FindBy(xpath="//button[text()='Accept']")
	WebElement cookieAcceptBtn;
	
	@FindBy(id="amount")
	WebElement amountTxtBx;
	
	@FindBy(xpath="//input[@id='midmarketFromCurrency']/parent::div")
	WebElement fromCurrencyHeader;
	
	@FindBy(id="midmarketFromCurrency")
	WebElement fromCurrency;
	
	@FindBy(xpath="//input[@id='midmarketToCurrency']/parent::div")
	WebElement toCurrencyHeader;
	
	@FindBy(id="midmarketToCurrency")
	WebElement toCurrency;
	
	@FindBy(xpath="//ul[@id='midmarketFromCurrency-listbox']/li[1]")
	WebElement fromCurrencyFirstOption;
	
	@FindBy(xpath="//ul[@id='midmarketToCurrency-listbox']/li[1]")
	WebElement toCurrencyFirstOption;

	@FindBy(xpath="//button[text()='Convert']")
	WebElement convertBtn;
	
	public void acceptCookie() {
		log.info("Accepting the cookies");
		cookieAcceptBtn.click();
	}
	
	
	public void setAmount(String amount) throws InterruptedException {
		//<TODO> Wait for element to be interactable
		log.info("Setting the amount value");
		Thread.sleep(2000);
		amountTxtBx.sendKeys(amount);
	}
	
	public void setFromCurrency(String currency) throws InterruptedException {
		log.info("Setting the From Currency to:" + currency);
		fromCurrencyHeader.click();
		Thread.sleep(1000);
		
		fromCurrency.sendKeys(currency);
		fromCurrencyFirstOption.click();
	}
	
	public void setToCurrency(String currency) {
		log.info("Setting the To Currency value: "  +currency);
		toCurrencyHeader.click();
		toCurrency.sendKeys(currency);
		toCurrencyFirstOption.click();
	}
	
	public void clickConvert() {
		log.info("Converting....");
		convertBtn.click();
	}
	
	public String getTitle() {
		return driver.getTitle();
	}
	
}

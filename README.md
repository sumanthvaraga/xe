# XE



## Getting started

To make it easy for you to get started running the project, here's a list of recommended next steps.


## Pre-req

Java 8, Maven, IDE(IntelliJ, Eclipse)


## Test and Deploy

To Execute the test, follow the steps

- Run the testng.xml **(or)**
- Open the test class and run the test directly from ide. **(or)**
- Run ''mvn clean test'' from command line.

No need to pass any arguments. Default browser is set to Chrome

## Reporting and logs

- Log4j is used and configured. Logs are stored under ./logs folder
- Extent Reports are used for test reporting and dashboard. Can be accessed from ./extent-reports folder